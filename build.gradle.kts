import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

object Module {
    const val jvmTarget = "11"
    const val namespace = "io.pivotal"
    const val version = "0.0.1-SNAPSHOT"
}

buildscript {
    repositories {
        mavenCentral()
    }
}

allprojects {
    repositories {
        mavenCentral()
        jcenter()
    }
}


plugins {
    id("org.springframework.boot") version ("2.2.5.RELEASE") apply (false)
    id("io.spring.dependency-management") version ("1.0.9.RELEASE") apply (false)
    kotlin("jvm") version "1.3.61" apply (false)
    kotlin("plugin.spring") version "1.3.61" apply (false)
}

subprojects {

    repositories {
        mavenCentral()
        jcenter()
        mavenLocal()
    }


    group = Module.namespace
    version = Module.version

    tasks.withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = Module.jvmTarget
        }
    }

    tasks.withType<JavaCompile> {
        sourceCompatibility = Module.jvmTarget
        targetCompatibility = Module.jvmTarget
    }

    tasks.withType<Jar>() {
        archiveBaseName.set("spring-rsocket-${project.name}")
    }

    apply {
        plugin("io.spring.dependency-management")
        plugin("org.jetbrains.kotlin.jvm")
        plugin("java-library")
        plugin("maven-publish")
        plugin("org.jetbrains.kotlin.plugin.spring")
    }

    val implementation = configurations.getByName("implementation")
    val testImplementation = configurations.getByName("testImplementation")

    dependencies {
        implementation(kotlin("stdlib"))
        implementation(kotlin("reflect"))
        implementation("org.slf4j:slf4j-api")
        implementation(platform("org.springframework.boot:spring-boot-dependencies:2.2.5.RELEASE"))
        testImplementation("org.junit.jupiter:junit-jupiter-api")
        testImplementation("org.junit.jupiter:junit-jupiter-params")
    }

    tasks.withType<Test> {
        systemProperty("junit.jupiter.extensions.autodetection.enabled", "true")
        systemProperty("junit.jupiter.testinstance.lifecycle.default", "per_class")
        useJUnitPlatform {
            excludeEngines.add("junit-vintage")
        }
    }
}

// compile group: 'org.slf4j', name: 'slf4j-api', version: '2.0.0-alpha1'
