package training.pivotal.commons

import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Inline function to retrieve a logging facade based on a class.
 */
inline fun <reified T> loggerOf(): Logger = LoggerFactory.getLogger(T::class.java)
