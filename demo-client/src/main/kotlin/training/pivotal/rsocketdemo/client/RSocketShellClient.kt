package training.pivotal.rsocketdemo.client

import org.springframework.beans.factory.annotation.Value
import org.springframework.messaging.rsocket.RSocketRequester
import org.springframework.messaging.rsocket.retrieveFlux
import org.springframework.messaging.rsocket.retrieveMono
import org.springframework.shell.Availability
import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import org.springframework.shell.standard.ShellMethodAvailability
import org.springframework.shell.standard.ShellOption
import reactor.core.Disposable
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import training.pivotal.commons.loggerOf
import training.pivotal.rsocketdemo.data.Message
import training.pivotal.rsocketdemo.data.Route
import java.time.Duration
import javax.annotation.PostConstruct


@ShellComponent
class RSocketShellClient(
        private val requestBuilder: RSocketRequester.Builder,
        @Value("\${rsocketdemo.host:localhost}") private val host: String,
        @Value("\${rsocketdemo.port:7070}") private val port: Int,
        @Value("\${rsocketdemo.client.autoConnect:false}") private val autoConnect: Boolean
) {

    private lateinit var rscocket: RSocketRequester
    private var connected = false


    @ShellMethod(value = "Stop streaming", key = ["s"])
    fun stop() {
        log.info("Stopping the incoming stream...")
        disposable?.takeUnless { it.isDisposed }
                ?.dispose()
                ?.also { log.info("Stream stopped.") }
    }

    @PostConstruct
    internal fun init() {
        if (autoConnect) {
            connect()
        } else log.warn("""
            Client not connected. Use the connect command.
        """.trimIndent())
    }

    @ShellMethod("Connect to demo server")
    fun connect(@ShellOption("--reconnect") reConnect: Boolean = false) {

        if (connected && reConnect) {
            disconnect()
        }

        if (connected) {
            log.info("Connected")
            return
        }

        log.info("Connecting to demo server: $host:$port")
        rscocket = requestBuilder.connectTcp(host, port).block() ?: error("Unable to build connection to demo server.")
        connected = true
    }

    private fun connectionAvailable(): Availability = when {
        connected -> Availability.available()
        else -> Availability.unavailable("You are not connected")
    }

    @ShellMethod("Disconnects from the the demo server.")
    @ShellMethodAvailability("connectionAvailable")
    fun disconnect() {

        if (!connected) {
            return
        }

        log.info("Disconnecting from server...")
        stop()
        rscocket.rsocket().dispose()
        connected = false

        log.info("Disconnected from server.")

    }

    @ShellMethod("Stream some settings to the server. Stream of responses will be printed.")
    @ShellMethodAvailability("connectionAvailable")
    fun channel() {

        val settings = Flux.concat(
                Mono.just(Duration.ofSeconds(1)),
                Mono.just(Duration.ofSeconds(3)).delayElement(Duration.ofSeconds(5)),
                Mono.just(Duration.ofSeconds(5)).delayElement(Duration.ofSeconds(15)))
                .doOnNext { d -> log.info("\nSending setting for {}-second interval\n", d.seconds) }

        disposable?.dispose()

        disposable = rscocket
                .route(Route.CHANNEL)
                .data(settings)
                .retrieveFlux(Message::class.java)
                .subscribe { message: Message? -> log.info("Received: {}\n(Type 's' to stop.)", message) }
    }

    @ShellMethod("Sends a request and wait for response.")
    @ShellMethodAvailability("connectionAvailable")
    fun requestResponse() {

        log.info("Sending request and waiting for response...")

        val response = rscocket
                .route("request-response")
                .data(Message(CLIENT, REQUEST))
                .retrieveMono<Message>()
                .block()

        log.info("RESPONSE: {}", response)

    }

    @ShellMethod("Sends one request. Many response will be printed to the screen.")
    @ShellMethodAvailability("connectionAvailable")
    fun stream() {

        log.info("\n\n**** Request-Stream\n**** Send one request.\n**** Log responses.\n**** Type 's' to stop.")

        disposable = rscocket
                .route(Route.STREAM)
                .data(Message(CLIENT, STREAM))
                .retrieveFlux<Message>()
                .subscribe { log.info("RECEIVED: {}\n(Type `s` to stop.)", it) }
    }

    @ShellMethod("Sends a request", key = ["fireAndForget", "fire", "f"])
    @ShellMethodAvailability("connectionAvailable")
    fun fireAndForget() {

        log.info("\nFire-And-Forget. Sending one request. Expect no response (check server console log)...")

        rscocket.route(Route.FIRE_AND_FORGET)
                .data(Message(CLIENT, FIRE_AND_FORGET))
                .send()
                .block()
    }

    companion object {
        private const val CLIENT = "Client"
        private const val REQUEST = "Request"
        private const val FIRE_AND_FORGET = "Fire-And-Forget"
        private const val STREAM = "Stream"
        private var disposable: Disposable? = null
        private val log = loggerOf<RSocketShellClient>()
    }

}
