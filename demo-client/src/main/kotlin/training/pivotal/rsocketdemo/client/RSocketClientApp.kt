package training.pivotal.rsocketdemo.client

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class RSocketClientApp

fun main(args: Array<String>) {
    runApplication<RSocketClientApp>(* args)
}
