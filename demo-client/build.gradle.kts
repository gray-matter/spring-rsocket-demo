
description = "Demo RSocket server client application"

apply(plugin = "org.springframework.boot")

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-rsocket")
    implementation("org.springframework.shell:spring-shell-starter:2.0.1.RELEASE")
    implementation(project(":commons"))
    implementation(project(":demo-data"))
}
