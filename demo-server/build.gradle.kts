



description = "Demo RSocket server application"

apply(plugin = "org.springframework.boot")

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-rsocket")
    implementation(project(":commons"))
    implementation(project(":demo-data"))
}
