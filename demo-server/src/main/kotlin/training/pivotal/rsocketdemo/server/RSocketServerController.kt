package training.pivotal.rsocketdemo.server

import training.pivotal.commons.loggerOf
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.stereotype.Controller
import reactor.core.publisher.Flux
import training.pivotal.rsocketdemo.data.Message
import training.pivotal.rsocketdemo.data.Route
import java.time.Duration


@Controller
class RSocketServerController {


    /**
     * This @MessageMapping is intended to be used "request → response" style.
     * For each Message received, a new Message is returned with ORIGIN=Server and INTERACTION=Request-Response.
     * @param request
     * @return Message
     */
    @MessageMapping(Route.REQUEST_RESPONSE)
    fun requestResponse(request: Message): Message {
        log.info("Received request-response request: {}", request)
        return Message(SERVER, RESPONSE)
    }


    /**
     * This @MessageMapping is intended to be used "fire --> forget" style.
     * When a new CommandRequest is received, nothing is returned (void)
     * @param request
     * @return
     */
    @MessageMapping(Route.FIRE_AND_FORGET)
    fun fireAndForget(request: Message?) {
        log.info("Received fire-and-forget request: {}", request)
    }

    /**
     * This @MessageMapping is intended to be used "subscribe --> stream" style.
     * When a new request command is received, a new stream of events is started and returned to the client.
     * @param request
     * @return
     */
    @MessageMapping(Route.STREAM)
    fun stream(request: Message): Flux<Message> {
        log.info("Received stream request: {}", request)
        return Flux
                // Flux emitting one element every second.
                .interval(Duration.ofSeconds(1))
                // Create flux of new messages
                .map { index -> Message(SERVER, STREAM, index) }
                // Use the flux logger to log each event.
                .log()
    }


    /**
     * This @MessageMapping is intended to be used "stream <--> stream" style.
     * The incoming stream contains the interval settings (in seconds) for the outgoing stream of messages.
     * @param settings
     * @return
     */
    @MessageMapping(Route.CHANNEL)
    fun channel(settings: Flux<Duration>): Flux<Message> {
        return settings
                .doOnNext { d -> log.info("Frequency setting is {} seconds.", d.seconds) }
                .switchMap { d: Duration ->
                    Flux.interval(d)
                            .map { index -> Message(SERVER, CHANNEL, index) }
                            .log()
                }
    }

    companion object {

        private const val SERVER = "Server"
        private const val RESPONSE = "Response"
        private const val STREAM = "Stream"
        private const val CHANNEL = "Channel"

        private val log = loggerOf<RSocketServerController>()

    }

}
