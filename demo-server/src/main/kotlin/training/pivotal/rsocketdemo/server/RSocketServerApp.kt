package training.pivotal.rsocketdemo.server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class RSocketServerApp

fun main(args: Array<String>) {
    runApplication<RSocketServerApp>(* args)
}
