package training.pivotal.rsocketdemo.server

import io.rsocket.RSocketFactory
import io.rsocket.RSocketFactory.ServerRSocketFactory
import org.springframework.boot.rsocket.server.ServerRSocketFactoryProcessor
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component


@Component
@Profile("resumption")
class RSocketServerResumptionConfig : ServerRSocketFactoryProcessor {
    override fun process(factory: ServerRSocketFactory?): ServerRSocketFactory {
        factory ?: error("No factory passed to processor")
        return factory.resume()
    }
}
