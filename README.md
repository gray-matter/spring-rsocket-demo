# RSocket Spring Demo

This is demo project taken from  [_Getting Started With RSocket: Boot Server_](https://spring.io/blog/2020/03/02/getting-started-with-rsocket-spring-boot-server) 

The project consist of the following modules: 

1. [`commons`](commons) - Any common utilities which has noting to do with the problem domain, such as logging etc.
2. [`demo-client`](demo-client) - A demo client shell connecting to the demo server,
3. [`demo-server`](demo-server) - A demo RSocket server
4. [`demo-data`](demo-data) - Messages and routes shared by the client and the server.

> The original java source code can be found here: https://github.com/benwilcock/spring-rsocket-demo



## System Requirements

- Operating System - Any OS capable of running java.
- Java 11 (JDK)



## How to Run

1. Clone the the project (if you haven’t done it yet) using git:

   ```shell
   git clone git@gitlab.com:bitdrop/spring-rsocket-demo.git
   ```

2. Change into the `spring-rocket-demo` folder.

3. Run the following command: 

   ```shell
   gradlew assemble
   ```

4. Open a new command console in the `spring-rocket-demo` folder, and run the server.

   ```shell
   java -jar demo-server\build\libs\spring-rsocket-demo-server-0.0.1-SNAPSHOT.jar --spring.profiles.active=resumption
   ```

5. Run the following to start the client:

   ```shell
   java -jar demo-client\build\libs\spring-rsocket-demo-client-0.0.1-SNAPSHOT.jar
   ```

   

To see a list of available commands, execute the `help` command in the shell, you should see something similar: 

<img src="README.assets/image-20200408173112806.png" alt="image-20200408173112806" style="zoom:67%;" />