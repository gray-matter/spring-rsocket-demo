package training.pivotal.rsocketdemo.data

import java.time.Instant

data class Message(val origin: String, val interaction: String, val index: Long, val created: Long) {

    constructor(origin: String, interaction: String) : this(
            origin,
            interaction,
            index = 0L,
            created = Instant.now().epochSecond
    )

    constructor(origin: String, interaction: String, index: Long) : this(
            origin,
            interaction,
            index,
            created = Instant.now().epochSecond
    )
}
