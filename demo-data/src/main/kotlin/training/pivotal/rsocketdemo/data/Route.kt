package training.pivotal.rsocketdemo.data

object Route {
    const val REQUEST_RESPONSE = "request-response"
    const val FIRE_AND_FORGET = "fire-and-forget"
    const val STREAM = "stream"
    const val CHANNEL = "channel"
}
