rootProject.name = "spring-rsocket-demo"


include("demo-client")
include("demo-server")
include("demo-data")
include("commons")
